import React, { useEffect, useState } from "react";
import { getMQG } from "./API/utils";
import "./App.css";

function App() {
  const [salesList, setSalesList] = useState([]);
  const [isLoading, setIsLoading] = useState(false);

  useEffect(() => {
    let ignore = false;
    setIsLoading(true);

    async function fetchData() {
      const MQG = await getMQG();
      const data = await MQG.QueryView()
        .attribute("department", "product.department.label")
        .attribute("city", "location.city.label")
        .attribute("month", "calendar.month.label")
        .measure("sales", "units", "TOTAL")
        .groupBy({ product: "department", location: "city", calendar: "month" }) //group the sales units by department
        .get();
      if (!ignore) {
        setSalesList(data);
        setIsLoading(false);
      }
    }

    fetchData();
    return () => {
      ignore = true;
    };
  }, []);

  if (isLoading) {
    return <h3>Loading..</h3>;
  }

  return (
    <div className="App">
      <table>
        <thead>
          <tr>
            {salesList[0] &&
              Object.keys(salesList[0]).map((column, index) => (
                <th key={index}>{column}</th>
              ))}
          </tr>
        </thead>
        <tbody>
          {salesList.map((salesItem, index) => {
            return (
              <tr key={index}>
                {Object.values(salesItem).map((levelValue, index) => (
                  <td key={index}>{levelValue}</td>
                ))}
              </tr>
            );
          })}
        </tbody>
      </table>
    </div>
  );
}

export default App;
