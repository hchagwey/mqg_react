import MQU from "./MQU";

export async function getMQG() {
  const MQG = await MQU.init({
    url: "http://localhost:8080/OLAP",
  });
  return MQG;
}
